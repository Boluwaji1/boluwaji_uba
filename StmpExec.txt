#============================================================
# IMPORTANT: Auto Generated Header. DO NOT CHANGE MANUALLY.
# Product: Finacle Core
# Category: Event Based
# Script: StmpExec.scr
# Type: ACHCI|default
# Description: ACHCI - ACHCI
# Author: Adewale.Adelekun
# Date: 17/07/20 15:05
# Modification Log:
#============================================================

<--START
CHECKSUM="cfa414dea98bd18dbf861807fb3b42dc08798c23742ac5b4d1f5cd169aa0c29f"
	# Script body goes here
	
	TRACE ON
	sv_a = urhk_b2k_PrintRepos("BANCS")
	PRINT(sv_a)
	#--------------------#
	# Repository Creation
	#--------------------#
	IF (REPEXISTS("CUST") == 0) THEN
	#{
		CREATEREP("CUST")
		#}
		
	#}
	ENDIF
	
	
	
	IF (CLASSEXISTS("CUST", "STMP") == 0) THEN
	#{
		CREATECLASS("CUST", "STMP", 5)
	#}
	ENDIF
	
	
	CUST.STMP.funcCode=BANCS.INPUT.funcCode
	PRINT(CUST.STMP.funcCode)
	CUST.STMP.AcctNum = BANCS.INPUT.foracid
	PRINT(CUST.STMP.AcctNum)
	CUST.STMP.ver_flg = ""
	CUST.STMP.del_flg = ""
	IF(CUST.STMP.AcctNum== "")THEN
	#{
		
		#{
		sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
		sv_x = urhk_SetOrbOut("Error_1|ERR^PLEASE PROVIDE VALID ACCOUNT NUMBER ^AcctNum")
		EXITSCRIPT
	#}
	ENDIF
	#=========================== USER VALIDATION  =======================================================
	BANCS.INPARAM.BINDVARS = BANCS.STDIN.userId
	sv_q = "vwkc,rolid|select user_work_class,role_id from tbaadm.upr "
	sv_q = sv_q + "where user_id = ?SVAR"
	sv_q = sv_q + "AND del_flg != 'Y' "
	PRINT(sv_q)
	sv_r = urhk_dbSelectWithBind(sv_q)
	PRINT(sv_r)
	IF (sv_r != 0) THEN
	#{
		
	#}
	ENDIF
	PRINT(BANCS.OUTPARAM.vwkc)
	sv_l = TRIM(BANCS.OUTPARAM.rolid)
	PRINT(sv_l)
	sv_p = BANCS.OUTPARAM.vwkc
	PRINT(sv_p)
	#{
	IF (sv_p == "038") THEN
	#{
		PRINT(sv_p)
		#}
	ELSE
		#{
		sv_m= "Unauthorized User"
		sv_u = urhk_SetOrbOut("SuccessOrFailure|N")
		sv_u = urhk_SetOrbErr(sv_m)
		EXITSCRIPT
	#}
	ENDIF
	
	#}
	#==================================  ADD ============================================
	IF (CUST.STMP.funcCode == "A") THEN
	#{
		sv_q = "acid,custid,cifid,acctname,AcctNum|select acid,cust_id,cif_id,acct_name,foracid from tbaadm.gam where foracid = '" + CUST.STMP.AcctNum + "'"
		sv_q = sv_q + "AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
		PRINT(sv_q)
		sv_d = urhk_dbSelectWithBind(sv_q)
		PRINT(sv_d)

		IF (sv_d == "1") THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ ACCOUNT DOES NOT EXIST^AcctNum")
				EXITSCRIPT
				
			#}
			ENDIF

		CUST.STMP.acid = BANCS.OUTPARAM.acid
		PRINT(CUST.STMP.acid)
		CUST.STMP.custid = BANCS.OUTPARAM.custid
		PRINT(CUST.STMP.custid)
		CUST.STMP.acctname = BANCS.OUTPARAM.acctname
		PRINT(CUST.STMP.acctname)
		CUST.STMP.AcctNum = BANCS.OUTPARAM.AcctNum
		PRINT(CUST.STMP.AcctNum)
		
		sv_a = " Counter|select count(*) from custom.sea where foracid= '" + CUST.STMP.AcctNum + "' AND ACID = '"+CUST.STMP.acid+"' AND ACCT_NAME = '"+CUST.STMP.acctname+"'"
		sv_a = sv_a  + "AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
		PRINT(sv_a)
		sv_d = urhk_dbSelectWithBind(sv_a)
		PRINT(sv_d)
		IF (sv_d == 0) THEN
		#{
			sv_m = CINT(BANCS.OUTPARAM.Counter)
			PRINT(sv_m)
			IF (sv_m >0) THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ EXEMPTION ALREADY EXIST ON THIS ACCOUNT^AcctNum")
				EXITSCRIPT
				
			#}
			ENDIF
		#}
		ENDIF
		
		sv_c = " ver_flg,del_flg|select VER_FLG,DEL_FLG from custom.STDEX where foracid= '" + CUST.STMP.AcctNum + "'"
		sv_c = sv_c  + "AND bank_id = '" + BANCS.STDIN.contextBankId +  "' AND rownum <2"
		PRINT(sv_c)
		sv_d = urhk_dbSelectWithBind(sv_c)
		PRINT(sv_d)
		IF (sv_d == 0) THEN
		#{
			CUST.STMP.ver_flg = BANCS.OUTPARAM.ver_flg
			CUST.STMP.del_flg = BANCS.OUTPARAM.del_flg
			
		#}
		ENDIF
		IF ((CUST.STMP.ver_flg == "N") AND (CUST.STMP.del_flg == "N")) THEN
		#{
			sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
			sv_x = urhk_SetOrbOut("Error_1|ERR^ ACCOUNT EXISTS PENDING VERIFICATION^AcctNum")
			EXITSCRIPT
		#}
		ENDIF
		sv_a = "INSERT INTO custom.STDEX values('"+ CUST.STMP.AcctNum+"','"+CUST.STMP.acid+"','"+CUST.STMP.acctname+"','"
		sv_a = sv_a + BANCS.STDIN.userId + "',SYSDATE,'' ,'' ,'' ,'' ,'" + BANCS.STDIN.mySolId + "','N', 'N','" + BANCS.STDIN.contextBankId + "')"
		PRINT(sv_a)
		sv_d = urhk_dbSQLWithBind(sv_a)
		sv_d = urhk_dbSQLWithBind("commit")
		PRINT(sv_d)
		
		
		IF (sv_d == 0) THEN
		#{
			sv_l = urhk_SetOrbOut("SuccessOrFailure|Y")
			sv_a = urhk_SetOrbOut("RESULT_MSG|RECORD SUCCESSFULLY ADDED")
			EXITSCRIPT
		#}
		ENDIF
	#}
	ENDIF
	#============================================ DELETE ================================
	IF (CUST.STMP.funcCode == "D") THEN
	#{
		sv_c = "del_flg|select DEL_FLG from custom.STDEX where foracid= '" + BANCS.INPUT.foracid + "' AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
		PRINT(sv_c)
		sv_d = urhk_dbSelectWithBind(sv_c)
		PRINT(sv_d)
		IF (sv_d == 0) THEN
		#{
			CUST.STMP.del_flg = BANCS.OUTPARAM.del_flg
			
			IF (CUST.STMP.del_flg == "Y") THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ RECORD HAS BEEN DELETED^AcctNum")
				EXITSCRIPT
				
			#}
			ENDIF
		#}
		ENDIF
		sv_a = " Counter|select count(*) from custom.sea where foracid= '" + BANCS.INPUT.foracid + "' AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
		PRINT(sv_a)
		sv_d = urhk_dbSelectWithBind(sv_a)
		PRINT(sv_d)
		IF (sv_d == 0) THEN
		#{
			sv_m = CINT(BANCS.OUTPARAM.Counter)
			PRINT(sv_m)
			IF (sv_m == 0) THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^RECORD DOES NOT EXIST ^AcctNum")
				EXITSCRIPT
			#}
			ENDIF
		#}
		ENDIF
		sv_d="UPDATE custom.STDEX set RCRE_USER_ID  = '" + BANCS.STDIN.userId+ "' ,RCRE_TIME = SYSDATE,VER_ID = '" + BANCS.STDIN.userId+ "',"
		sv_d = sv_d + " VER_TIME =SYSDATE,DEL_FLG ='Y',VER_FLG ='N' where foracid = '" + CUST.STMP.AcctNum + "' AND VER_FLG ='Y' AND del_flg='N' "
		sv_f = urhk_dbSQLWithBind(sv_d)
		sv_f = urhk_dbSQLWithBind("commit")
		PRINT(sv_f)
		IF (sv_f == 0) THEN
		#{
			sv_l = urhk_SetOrbOut("SuccessOrFailure|Y")
			sv_a = urhk_SetOrbOut("RESULT_MSG|RECORD DELETED AWAITING VERIFICATION")
		#}
		ENDIF
		
		EXITSCRIPT
	#}
	ENDIF
	#============================================ VERIFY ================================
	IF (CUST.STMP.funcCode == "V") THEN
	#{
		sv_k = "rcre_user_id,ver_flg,del_flg,acctname,acid|select RCRE_USER_ID,VER_FLG,DEL_FLG,ACCT_NAME,ACID from custom.STDEX where foracid= '" + BANCS.INPUT.foracid + "' AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
		sv_k = sv_k  + "AND rownum <2"
		PRINT(sv_K)
		sv_d = urhk_dbSelectWithBind(sv_k)
		PRINT(sv_d)
		IF (sv_d==0) THEN
		#{
			CUST.STMP.rcre_user_id = BANCS.OUTPARAM.rcre_user_id
			PRINT(CUST.STMP.rcre_user_id)
			CUST.STMP.ver_flg = BANCS.OUTPARAM.ver_flg
			PRINT(CUST.STMP.ver_flg)
			CUST.STMP.del_flg = BANCS.OUTPARAM.del_flg
			PRINT(CUST.STMP.del_flg)
			CUST.STMP.acctname = BANCS.OUTPARAM.acctname
			PRINT(CUST.STMP.acctname)
			CUST.STMP.acid = BANCS.OUTPARAM.acid
			PRINT(CUST.STMP.acid)
			
			IF (CUST.STMP.rcre_user_id == BANCS.STDIN.userId) THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ SAME USER CANNOT VERIFY^RCRE_USER_ID")
				EXITSCRIPT
			#}
			ENDIF
			IF ((CUST.STMP.ver_flg == "Y") AND (CUST.STMP.del_flg == "N"))THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ RECORD ALREADY VERIFIED^RCRE_USER_ID")
				EXITSCRIPT
			#}
			ENDIF
			IF ((CUST.STMP.ver_flg == "Y") AND (CUST.STMP.del_flg == "Y"))THEN
			#{
				sv_l = urhk_SetOrbOut("SuccessOrFailure|N")
				sv_x = urhk_SetOrbOut("Error_1|ERR^ RECORD DELETED AND ALREADY VERIFIED^RCRE_USER_ID")
				EXITSCRIPT
			#}
			ENDIF
			
			#{
			sv_d="UPDATE custom.STDEX set LCHG_USER_ID  = '" + BANCS.STDIN.userId+ "' ,LCHG_TIME = SYSDATE,VER_ID = '" + BANCS.STDIN.userId+ "',VER_TIME =SYSDATE,VER_FLG ='Y' where foracid = '" + CUST.STMP.AcctNum + "' AND ver_flg='N' "
			sv_r = urhk_dbSQLWithBind(sv_d)
			sv_r = urhk_dbSQLWithBind("commit")
			PRINT(sv_r)
			IF (sv_r == 0)THEN
			#{
				IF (CUST.STMP.del_flg == "N") THEN
				#{
					sv_a = "INSERT INTO custom.sea values('"+CUST.STMP.AcctNum+"','"+CUST.STMP.acid+"','"+CUST.STMP.acctname+"',SYSDATE,'SYSTEM','" + BANCS.STDIN.contextBankId + "')"
					PRINT(sv_a)
					sv_x = urhk_dbSQLWithBind(sv_a)
					sv_x = urhk_dbSQLWithBind("commit")
					PRINT(sv_x)
					#}
					IF (sv_x == 0) THEN
					#{
						sv_l = urhk_SetOrbOut("SuccessOrFailure|Y")
						sv_a = urhk_SetOrbOut("RESULT_MSG|STAMP DUTY EXEMPTION SUCCESSFUL")
						#}
						EXITSCRIPT
					#}
					ENDIF
				#}
				ENDIF
				IF (CUST.STMP.del_flg == "Y") THEN
				#{
					sv_a = "DELETE CUSTOM.SEA WHERE FORACID ='"+BANCS.INPUT.foracid +"'AND bank_id = '" + BANCS.STDIN.contextBankId + "'"
					sv_r = urhk_dbSqlWithBind(sv_a)
					sv_r = urhk_dbSQLWithBind("commit")
					PRINT(sv_r)
					IF (sv_r == 0) THEN
					#{
						sv_l = urhk_SetOrbOut("SuccessOrFailure|Y")
						sv_a = urhk_SetOrbOut("RESULT_MSG|RECORD DELETED SUCCESSFULLY")
						#}
						EXITSCRIPT
					#}
					ENDIF
				#}
				ENDIF
			#}
			ENDIF
		#}
		ENDIF
	#}
	ENDIF
	
	
	
	
	TRACE OFF
END-->
